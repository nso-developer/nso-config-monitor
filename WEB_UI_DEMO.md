## Config Monitor in Web UI

### Table of contents
 - [Config Monitor Panel](#config-monitor-panel)
 - [Running Inspection](#running-inspection)
 - [Browsing Remote Reports](#browsing-remote-reports)
 - [Browsing Inspection Historical Data](#browsing-inspection-historical-data)
 - [Changing Stats History Size](#changing-stats-history-size)

### Config Monitor Panel

The NSO Config Monitor can be run from Web UI as well as in CLI. To open NSO Web UI in your browser navigate to URL
_https://<NSO-Server-IP>:8080_ and enter your login credentials. That will bring you to the Web UI main page.
Select _Configuration Editor_, then in the MODULES section select _config-monitor:config-monitor_ module.
That will bring you to the Config Monitor working panel:

<img src="resources/config-monitor-panel.png" width="600">

The panel contains 4 sections:
 - _stats-size_ configuration
 - _inspect-config_ action button
 - _report-files_ list
 - _config-stats_ historical data

Let's navigate to each section for more details.

### Running Inspection

Select _inspect-config_ action button to get to inspection configuration panel:

<img src="resources/inspect-config.png" width="600">

Select _outformat_ of the config diff report: _xml_ or _cli_.

Select _skip-inspection_ option, if you want to limit inspection to one section of analysis:
_unmanaged_ or _non-automated_ parts of configuration.

Define _device-list_ to run the inspection on. It could be either simple list of devices or device group.

Enable _save_ flag, if you want to save all the generated reports to the NSO server file system and optionally
full path to report file directory in the _to-dir_ text field.

When all the input parameters are set, press _Run inspection-config action_ button.
Wait for inspection process to finish, then browse _result_ panel.

<img src="resources/inspection-results.png" width="600">

### Browsing Remote Reports

The operational data displayed in the Web UI automatically, it is webui-one feature. There is no action attached to the 
lines of the _report-files_ list. Therefore, the browsing of the report files involves some manual operation.
To copy report file path, find the report in the list and click corresponding line; that will give you file info panel,
from which you can copy the file path.

<img src="resources/report-file-info.png" width="600">

#### Browsing report from default location
When default report location is used, the file info displays partial file path, which starts from _config-inspection-reports_.
Copy that path and, while logged-in to the Web UI, build and enter file URL, which should look like in this example:
```
172.23.80.31:8080/config-inspection-reports/ios.info.txt
```

#### Browsing report from user defined location

When user defines report file location, the file info displays full path to the files. Copy that path for following it
download to local platform. There are multiple methods to download files from remote server. Here is an example with
_scp_ command, which uses SSH protocol:

```
scp 172.23.80.31:/home/ygorelik/nso-test/config-inspection-reports/xr-32.cfg.html /tmp/
open -a safari /tmp/xr-32.cfg.html &
```

### Browsing Inspection Historical Data

On the Config Monitor panel locate section _config-stats_, which lists all the devices that were inspected.
Select line for the device of interest, which brings you to details of collected stats. Here is an example:

<img src="resources/inspection-stats.png" width="600">

### Changing Stats History Size

On the Config Monitor panel locate section _stats-size_. Type in new size value then click the Commit Manager button
on the bottom menu. That will open _commit_ panel for the stats-size.

<img src="resources/stats-size-commit.png" width="600">

Check the old and new values for the _stats-size_ and, when satisfied, press _Commit_ button,
and then in the pop-up dialog _yes, commit_ button.