## NSO Config Monitor

### Table of contents
 - [Introduction](#introduction)
 - [Components](#components)
 - [Installation](#installation)
 - [Inspecting device configurations](#inspecting-device-configurations)
 - [Operational Data](#operational-data)
 - [Known Caveats](#known-caveats)

### Introduction

The NSO Config Monitor is targeted to discover and report unmanaged device configurations
as well as configuration changes that were introduced outside of NSO. 
It is designed to work as plug-in package named _config-monitor_, which can be installed and used
in customer based NSO installations.

### Components

The _config-monitor_ package includes the following components:

**Inspect Config Action**

This action performs two independent tasks:
1. Performs 'sync-from verbose dry-run' NSO action and builds:
   - Device configuration differences between NSO and actual device (diff).
   - Part of the device configuration that is not covered by NED model (unmanaged).
2. Based on current state of CDB calculates the following:
   - Part of the device configuration that belong to deployed services (automated).
   - Part of the device configuration that does not belong to any services (non-automated).
   - Builds HTML webpage that displays device configuration, where non-automated lines are colored in red.

**Operational Data**

Represent history of stats collected during device configuration inspections.

### Installation

#### Prerequisites

1. NSO 5.3 or higher version is required.
2. Python 3.6 or higher version is required.
3. In Python environment install additional component:
```commandline
pip install pyparsing
```

#### Procedure

Download the git repository of the Config Manager and then build the package files:
```commandline
git clone https://wwwin-github.cisco.com/ygorelik/nso-config-monitor.git
cd nso-config-monitor
source $NCS_DIR/ncsrc
# Initialize Python virtual environment if applicable
make
```

For Config Monitor deployment in already active NSO:
```commandline
cd /your-NSO-working-directory
cd packages
ln -s ~/nso-config-monitor/packages/config-nonitor 
cd -
$ ncs_cli -Cu admin
admin@ncs# packages reload
```

### Inspecting device configurations

To run inspection of device configurations execute this command from NSO CLI:
```commandline
admin@ncs# config-monitor inspect-config [device <device-list> | device-group <group>] \
           [outformat {xml | cli}] [save [ { to-dir user-specified-path } ]] \
           [skip-inspection {unmanaged | non-automated}]
```
Optional parameters are:
 - device or device-group - a list of specific devices or all devices in device group,
that will be inspected. If not specified, all managed devices will be inspected.
 - outformat  - with choice of _xml_ or _cli_; the default setting is _xml_.
 - save - set this option, if you want to save retrieved information (reports) in the file system.
If not set, the reports are printed to the NSO CLI console.
 - to-dir - set full path to a directory on the NSO server to store report files. If not set, the report files are
placed to _webui_ directory for capability to display them in a web browser over the NSO Web UI. 
This default report files location is defined as:
> /NSO/working/directory/packages/config-monitor/webui/config-inspection-reports

 - skip-inspection - set this optional parameter to _unmanaged_, if you would like to skip analysis of config differences and
unmanaged config; it is useful when device configurations are inspected off the network. Set this parameter
to _non-automated_ to skip analysis of non-automated part of device configuration.

While inspecting the device configurations the inspection time for each device is printed to the NSO CLI console.
At the end of console output the summary is printed. Here is an example where all the devices are inspected and
the reports are saved to file system in default locations:
```commandline
admin@ncs# config-monitor inspect-config save
Inspecting device ce0 ... Done in 4.88 seconds
Inspecting device ce1 ... Done in 5.14 seconds
Inspecting device ce2 ... Done in 5.12 seconds
Inspecting device ce3 ... Done in 5.11 seconds
Inspecting device ce4 ... Done in 5.27 seconds
Inspecting device ce5 ... Done in 5.41 seconds
Inspecting device ce6 ... Done in 3.88 seconds
Inspecting device ce7 ... Done in 4.11 seconds
Inspecting device ce8 ... Done in 4.07 seconds
Inspecting device p0 ... Done in 2.84 seconds
Inspecting device p1 ... Done in 2.77 seconds
Inspecting device p2 ... Done in 3.27 seconds
Inspecting device p3 ... Done in 2.91 seconds
Inspecting device pe0 ... Done in 3.66 seconds
Inspecting device pe1 ... Done in 3.2 seconds
Inspecting device pe2 ... Done in 5.75 seconds
Inspecting device pe3 ... Done in 3.0 seconds
result 

Devices inspected: 17
Devices out of sync: 6
All the devices in test are fully managed by NSO
Devices with non-automated config: 17

admin@ncs# 
```

#### Inspection reports

The Config Monitor produces the following files/reports in default or user defined directory:

   - <device-name>.diff.xml or <device-name>.diff.cli, based on _outformat_ option - these files store
     configuration _diff_ in case the NSO is not in sync with the device
   - <device-name>.info.txt - information about device configuration that is not managed by NED
   - <device-name>.ref.csv - table of leafs that were set as a result of services deployment; data stored in
     CSV format to be easily viewed in Excel or Google Spreadsheet
   - <device-name>.nref.csv - table of leafs that were configured outside the deployed services,
     for example, manual configuration, file in CSV format
   - <device-name>.nref.cfg - device config in NED native format, which is not part of any deployed services
   - <device-name>.nref.html - device running config in HTML format, where colored lines are not part of any
   deployed services; the files are generated only for these NEDs: cisco-ios-cli, cisco-iosxr-cli, cisco-iosxr-nc,
   and juniper-junos-nc. Note, the device running config is taken from NSO CDB, which is slightly different
   from the one taken from the device.

### Operational Data

#### Inspection stats

Each time the _inspect_config_ action is running, it produces in addition to information stored in files or
displayed some statistics, which is recorded in CDB as historic operational data. That data can be viewed in
NSO console or retrieved programmatically. To display the historic inspection execute this command in NSO CLI
prompt:
```commandline
admin@ncs# show config-monitor config-stats ce1
                                          LEAFS   LEAFS    LEAFS    CONFIG     
                                   IN     IN      IN       NOT IN   LINES      
DEVICE  INSPECTION TIME            SYNC   CONFIG  SERVICE  SERVICE  UNMANAGED  
-------------------------------------------------------------------------------
ce1     2022-05-04T00:35:15-00:00  false  337     33       26       0          
        2022-05-04T00:36:24-00:00  false  337     33       26       0          
```

The history length is configurable parameter; the default length is set to 10 records per device.
To change history length enter these commands at config prompt:
```commandline
admin@ncs(config)# config-monitor stats-size 8
admin@ncs(config)# commit
Commit complete.
```
When number of data in history is greater than _stats-size_, the history is truncated to most recent
inspections.

#### Report files

When reports are saved on the NSO hosting server, the report file list is recorded in CDB operational data.
These data can be viewed in the CLI; example:
```
admin@ncs# show config-monitor report-files
PATH                                       SIZE  TIME CREATED               
----------------------------------------------------------------------------
config-inspection-reports/ios.info.txt     89    2023-01-22T13:16:03-00:00  
config-inspection-reports/ios.nref.cfg     449   2023-01-22T13:16:06-00:00  
config-inspection-reports/ios.nref.csv     1852  2023-01-22T13:16:03-00:00  
config-inspection-reports/ios.nref.html    2434  2023-01-22T13:16:06-00:00  
config-inspection-reports/iosxr.info.txt   89    2023-01-22T13:16:07-00:00  
config-inspection-reports/iosxr.nref.cfg   127   2023-01-22T13:16:08-00:00  
config-inspection-reports/iosxr.nref.csv   440   2023-01-22T13:16:07-00:00  
config-inspection-reports/iosxr.nref.html  875   2023-01-22T13:16:08-00:00  
config-inspection-reports/iosxr.ref.csv    246   2023-01-22T13:16:07-00:00  
```
In this example all the files are written to default location, therefore only partial path is shown.
These partial paths then can be copied and used to build URL for displaying reports in a web browser.  

### Known Caveats

1. In cisco CLI NEDs, while parsing non-automated interfaces the 'no shutdown' statement is skipped (not colored in HTML).
2. The Config Monitor does not report config changes made to deployed services, as those changes do not affect
the corresponding refcount values. These config changes could be seen by using NSO built-in compliance tools.
3. Manual copy/paste operation is needed to display saved reports in a web browser.
