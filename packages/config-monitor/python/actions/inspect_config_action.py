# *************************************************************
#  NSO Config Monitor
#  Copyright 2022 Cisco Systems. All rights reserved
# *************************************************************
#  Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements. The ASF licenses this
# file to you under the Apache License, Version 2.0.
# You may not use this file except in compliance with the
# License.  You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# *************************************************************
import os
import ncs
import _ncs
import _ncs.cdb as _cdb
import socket
from ncs.dp import Action

from datetime import datetime
import textwrap
from collections import OrderedDict
import pyparsing as pp
import shutil
from pathlib import Path

from _ncs import ATTR_BACKPOINTER
try:
    from _ncs import ATTR_REFCOUNT
except ImportError:
    ATTR_REFCOUNT = 2147483650


class InspectConfig(Action):

    @Action.action
    def cb_action(self, uinfo, name, kp, _input, output, trans):
        """Called when the analyze-config-action is called
        The function is called with parameters:
            uinfo -- a UserInfo object
            name -- the tailf:snmp-action name (string)
            kp -- the keypath of the snmp-action (HKeypathRef)
            _input -- input node (maagic.Node)
            output -- output node (maagic.Node)
        """
        self.log.debug(f"Entered cb_action {name}")

        _ncs.dp.action_set_timeout(uinfo, 3600)  # extend default time to execute this action
        output.result = ""
        root = ncs.maagic.get_root(trans)
        if hasattr(_input, 'device') and _input.device:
            devices = _input.device.as_list()
        elif hasattr(_input, 'device_group') and _input.device_group:
            devices = root.devices.device_group[_input.device_group].member.as_list()
        else:
            devices = [k._key_str()[1:-1] for k in root.devices.device.keys()]

        self.log.info(f"Running inspect-config action on these devices: {devices}\n")

        s = socket.socket()
        _cdb.connect(s, _cdb.DATA_SOCKET, '127.0.0.1', _ncs.NCS_PORT, '/')
        _cdb.start_session2(s, _cdb.RUNNING, 0)

        if _input.save.exists():
            if _input.save.to_dir:
                report_dir = _input.save.to_dir
            else:
                file_dir = Path(__file__)
                nso_dir = file_dir.parent.parent.parent.parent.parent.parent.parent
                report_dir = os.path.join(nso_dir, 'packages', 'config-monitor', 'webui',
                                          'config-inspection-reports')
        devices_with_diff = 0
        devices_with_info = 0
        devices_with_nref = 0
        reports_written = []
        for device_name in devices:
            start_time = datetime.now()
            trans.maapi.prio_message(uinfo.username, f"Inspecting device {device_name} ... ")
            diff = info = ''
            if 'unmanaged' != _input.skip_inspection:
                diff, info = get_sync_from_diff(device_name, _input.outformat)
                if diff:
                    devices_with_diff += 1
                if info:
                    # For XR device check if info contains line:
                    #   Number of lines skipped          :      0
                    found_match = False
                    for line in info.split('\n', 3):
                        if 'Number of lines skipped' in line and ':' in line and\
                                line.split(':')[1].strip() == '0':
                            found_match = True
                            break
                    if not found_match:
                        devices_with_info += 1

            leaf_paths = {}
            device_set_leafs = {}
            device_not_ref_leafs = {}
            if 'non-automated' != _input.skip_inspection:
                # Collect all leafs in config
                start_path = "/devices/device{%s}/config" % device_name
                parse_children(leaf_paths, start_path, trans)
                path_max_len = 0
                val_max_len = 5
                nref_path_max_len = 0
                for path, value in leaf_paths.items():
                    ref_counter, ref_pointer = get_path_attr(path, trans, self.log)
                    if ref_counter > 0 or len(ref_pointer) > 0:
                        refs = {"value": value, "ref_counter": ref_counter, "ref_pointer": ref_pointer}
                        device_set_leafs[path] = refs
                        if len(path) > path_max_len:
                            path_max_len = len(path)
                        val_str = str(value)
                        if len(val_str) > val_max_len:
                            val_max_len = len(val_str)
                        continue

                    # Include keys to the set leafs and give them attributes of the list element
                    last_close_curly = path.rfind('}/')
                    leaf_name = path[last_close_curly+2:]
                    if '/' not in leaf_name:
                        list_elem_path = path[:last_close_curly + 1]
                        keys = get_list_keys(list_elem_path, trans)
                        if leaf_name in keys:
                            ref_counter, ref_pointer = get_path_attr(list_elem_path, trans, self.log)
                            if ref_counter > 0 or len(ref_pointer) > 0:
                                refs = {"value": value, "ref_counter": ref_counter, "ref_pointer": ref_pointer}
                                device_set_leafs[path] = refs
                                if len(path) > path_max_len:
                                    path_max_len = len(path)
                                val_str = str(value)
                                if len(val_str) > val_max_len:
                                    val_max_len = len(val_str)
                                continue

                    # Check if leaf does not exist or got default value, but not written to CDB, drop them
                    if path[-1] != '}' and \
                            (not _cdb.exists(s, path) or _cdb.is_default(s, path)):
                        continue

                    # Collect the rest of leafs, which are set, but not in service
                    device_not_ref_leafs[path] = value
                    if len(path) > nref_path_max_len:
                        nref_path_max_len = len(path)

            device = root.devices.device[device_name]
            ned_id = get_device_ned_id(device)
            if _input.save.exists():
                # Write collected data to file system
                if diff:
                    if 'xml' == _input.outformat:
                        write_to_file(report_dir, device_name, diff, "diff.xml", reports_written)
                    else:
                        write_to_file(report_dir, device_name, diff, "diff.cli", reports_written)

                if info:
                    write_to_file(report_dir, device_name, info, "info.txt", reports_written)

                leaf_report = "LEAF PATH,VALUE,REF_COUNT,REF_POINTERS\n"
                for (path, attrs) in device_set_leafs.items():
                    leaf_report += f"{path},{attrs['value']},{attrs['ref_counter']},{attrs['ref_pointer']}\n"
                if device_set_leafs:
                    write_to_file(report_dir, device_name, leaf_report, "ref.csv", reports_written)

                if device_not_ref_leafs:
                    devices_with_nref += 1
                    not_ref_leaf_report = "LEAF PATH WITH NO REFERENCES,VALUE"
                    for path in device_not_ref_leafs:
                        not_ref_leaf_report += f"\n{path},{device_not_ref_leafs[path]}"
                    write_to_file(report_dir, device_name, not_ref_leaf_report, "nref.csv", reports_written)

                    if config_compare_supported(ned_id):
                        nref_config = self.build_nref_config(device_name, device_not_ref_leafs, ned_id, trans)
                        if nref_config:
                            write_to_file(report_dir, device_name, ''.join(nref_config), "nref.cfg", reports_written)

                            html_config = build_running_config_html(device_name, nref_config, ned_id, self.log)
                            write_to_file(report_dir, device_name, html_config, "nref.html", reports_written)

            else:  # Output to console
                if 'unmanaged' != _input.skip_inspection:
                    if diff:
                        output.result += f"\nDevice {device_name} is out-of-sync. Configuration diff:\n\n{diff}"
                    else:
                        output.result += f"\nDevice {device_name} is in-sync\n"

                    if info:
                        output.result += f"\nDevice {device_name} sync-from info:\n\n{info}\n"

                if 'non-automated' != _input.skip_inspection:
                    output.result += \
                        f"\nDevice {device_name} leafs in service ({len(device_set_leafs) * 100 // len(leaf_paths)}%):\n"
                    if device_set_leafs:
                        output.result += (f"\n{'LEAF PATH'.ljust(path_max_len)}  "
                                          f"{'VALUE'.ljust(val_max_len)}  REF_COUNT  REF_POINTERS")
                    for (path, attrs) in device_set_leafs.items():
                        output.result += \
                            f"\n{path.ljust(path_max_len)}  {str(attrs['value']).ljust(val_max_len)}      "\
                            f"{str(attrs['ref_counter']).rjust(2)}     {attrs['ref_pointer']}"
                    if device_set_leafs:
                        output.result += '\n'

                    output.result += f"\nDevice {device_name} leafs not in services: {len(device_not_ref_leafs)}\n"
                    if device_not_ref_leafs:
                        devices_with_nref += 1
                        output.result += f"\n{'LEAF PATH'.ljust(nref_path_max_len)}  {'VALUE'}"
                        for path in device_not_ref_leafs:
                            output.result += f"\n{path.ljust(nref_path_max_len)}  {device_not_ref_leafs[path]}"

                        if config_compare_supported(ned_id):
                            nref_config = self.build_nref_config(device_name, device_not_ref_leafs, ned_id, trans)
                            if nref_config:
                                output.result += f"\n\nDevice {device_name} not in services config:\n\n{''.join(nref_config)}"

            write_oper_data(device_name, len(device_set_leafs),
                            len(device_not_ref_leafs), len(leaf_paths), diff, info, reports_written, self.log)

            time_elapsed = datetime.now() - start_time
            trans.maapi.prio_message(uinfo.username, f"Done in {round(time_elapsed.total_seconds(), 2)} seconds\n")
            # for device_name in devices:
        _cdb.end_session(s)
        s.close()

        if _input.save.exists():
            cleanup_oper_data(report_dir)

        output.result += f"\n\nDevices inspected: {len(devices)}"
        if 'unmanaged' != _input.skip_inspection:
            if devices_with_diff:
                output.result += f"\nDevices out of sync: {devices_with_diff}"
            else:
                output.result += "\nAll the devices in test are in-sync"
            if devices_with_info:
                output.result += f"\nDevices with unmanaged config: {devices_with_info}"
            else:
                output.result += "\nAll the devices in test are fully managed by NSO"

        if 'non-automated' != _input.skip_inspection:
            if devices_with_nref:
                output.result += f"\nDevices with non-automated config: {devices_with_nref}"
            else:
                output.result += "\nAll the devices in test are fully automated by NSO"
        output.result += "\n"

    def build_nref_config(self, device_name, device_not_ref_leafs, ned_id, trans):
        path_leaf_dict = {}
        for path in device_not_ref_leafs:
            path_and_leaf = path.rsplit('/', 1)
            test_path = path_and_leaf[0]
            leaf_name = path_and_leaf[1]

            # Skip list keys
            if test_path[-1] == '}':
                keys = get_list_keys(test_path, trans)
                if leaf_name in keys:
                    continue

            if test_path in path_leaf_dict:
                leaf_list = path_leaf_dict[test_path]
                leaf_list.append(leaf_name)
            else:
                leaf_list = [leaf_name]
                path_leaf_dict[test_path] = leaf_list

        config_list = []
        for path, leafs in path_leaf_dict.items():
            with ncs.maapi.single_write_trans('admin', 'system') as tr:
                try:
                    for leaf in leafs:
                        tr.delete(f"{path}/{leaf}")
                    cp = ncs.maapi.CommitParams()
                    if ned_id in ['cisco-ios-cli', 'cisco-iosxr-cli']:
                        cp.dry_run_native_reverse()
                        r = tr.apply_params(False, cp)
                        if r and 'device' in r:
                            cli = r['device'][device_name]
                            if cli not in config_list:
                                config_list.append(cli)
                            else:
                                self.log.info(f"\nNo native config on Path: {path}, Leafs: {leafs}")
                        else:  # got empty response - NSO defect CSCwb96864
                            cli = try_cli(device_name, path, leafs)
                            if cli and cli not in config_list:
                                config_list.append(cli)
                    elif ned_id in ['juniper-junos-nc']:
                        cp.dry_run_cli()
                        r = tr.apply_params(False, cp)
                        if r and 'local-node' in r:
                            cli = r['local-node']
                            cli = cli.replace('\n-', '\n ')
                            if cli not in config_list:
                                config_list.append(cli)
                            else:
                                self.log.info(f"\nNo cli config on Path: {path}, Leafs: {leafs}")
                    elif ned_id == 'cisco-iosxr-nc':
                        if hasattr(cp, 'dry_run_cli_c_reverse'):
                            cp.dry_run_cli_c_reverse()
                            r = tr.apply_params(False, cp)
                            if r and 'local-node' in r:
                                cli = r['local-node']
                                if cli not in config_list:
                                    config_list.append(cli)
                                else:
                                    self.log.info(f"\nNo cli-c config on Path: {path}, Leafs: {leafs}")
                        else:
                            cp.dry_run_cli()
                            r = tr.apply_params(False, cp)
                            if r and 'local-node' in r:
                                cli = r['local-node']
                                cli = cli.replace('\n-', '\n ')
                                if cli not in config_list:
                                    config_list.append(cli)
                                else:
                                    self.log.info(f"\nNo cli config on Path: {path}, Leafs: {leafs}")

                except _ncs.error.Error as er:
                    self.log.info(f"\nError on Path: {path}, Leafs: {leafs},\nException: {er}")
        return config_list


def config_compare_supported(ned_id):
    return ned_id and \
            ned_id in ['cisco-ios-cli',
                       'cisco-iosxr-cli',
                       'cisco-iosxr-nc',
                       'juniper-junos-nc',
                       ]


def get_device_ned_id(device):
    ned_id = ncs.application.get_ned_id(device)
    if ':' in ned_id:
        ned_id = ned_id.split(':')[0]
    pos = ned_id.rfind('-')
    if pos:
        ned_id = ned_id[:pos]
    return ned_id


def build_running_config_html(device_name, config_list, ned_id, logger):
    running_config = read_device_config(device_name)
    html_config = html_prefix % (device_name, device_name) + html_suffix
    if ned_id in ['cisco-ios-cli', 'cisco-iosxr-cli', 'cisco-iosxr-nc']:
        gen = config_reader(running_config)
        line = next(gen, '')
        root_node = ConfigNode(line)
        config_tree = config_to_tree(root_node, gen, logger)

        for config in config_list:
            match_gen = config_reader(config)
            if ned_id in ['cisco-ios-cli', 'cisco-iosxr-cli']:
                config_node = ConfigNode('config')
                match_tree = config_to_tree(config_node, match_gen, logger, -1)
            else:
                line = next(match_gen, '')
                config_node = ConfigNode(line)
                match_tree = config_to_tree(config_node, match_gen, logger)
            color_config(config_tree, match_tree, logger)

        colored_config = tree_to_config(config_tree)
        html_config = html_prefix % (device_name, device_name) + colored_config + html_suffix

    elif ned_id == 'juniper-junos-nc':
        config_tree = JunoConfig(config=running_config)
        for config in config_list:
            match_tree = JunoConfig(config=config)
            color_j_config(config_tree, match_tree, logger)
        html_config = html_prefix % (device_name, device_name) + config_tree.html_str() + html_suffix

    return html_config


def try_cli(device_name, path, leafs):
    with ncs.maapi.single_write_trans('admin', 'system') as tr:
        try:
            for leaf in leafs:
                tr.delete(f"{path}/{leaf}")
            cp = ncs.maapi.CommitParams()
            cp.dry_run_cli()
            r = tr.apply_params(False, cp)
            if 'local-node' in r:
                juno_cli = r['local-node']
                juno_cli = juno_cli.replace('\n-', '\n ')
                device_config_tree = JunoConfig(config=juno_cli)
                device = f"device {device_name}"
                config_tree = device_config_tree.sub_sections['devices'].sub_sections[device].sub_sections['config']
                statements = get_all_statements(config_tree, '')
                cli = ''
                for st in statements:
                    if st.endswith(';'):
                        st = st[:-1]
                    if st.endswith(' true'):
                        st = st.replace(' true', '')
                    elif st.endswith(' false'):
                        st = 'no ' + st.replace(' false', '')
                    cli += f"{st}\n"
                return cli
        except _ncs.error.Error as er:
            print(f"\nFailed Path: {path}, Leafs: {leafs}, Error: {er}")
    return None


def get_all_statements(config_tree, parent_statement):
    statements = []
    for statement in config_tree.statements:
        st = f"{parent_statement} {statement}"
        statements.append(st)
    for name, section in config_tree.sub_sections.items():
        st = f"{parent_statement} {name}" if parent_statement else name
        statements.extend(get_all_statements(section, st))
    return statements


html_prefix = '''<!DOCTYPE html>
<html>
  <head>
    <title>Configuration of device %s</title>
  </head>
  <body>
  <h3>Configuration of device %s</h3>
<pre>
'''

html_suffix = '''</pre>
  </body>
</html>
'''


def get_sync_from_diff(dev_name, out_format):
    with ncs.maapi.single_read_trans('admin', 'system') as tr:
        root = ncs.maagic.get_root(tr)
        dev = root.devices.device[dev_name]
        inp = dev.sync_from.get_input()
        inp.verbose.create()
        inp.dry_run.create()
        inp.dry_run.outformat = out_format
        res = dev.sync_from(inp)
        if out_format == 'xml':
            return res.result_xml, res.info
        else:
            return res.cli, res.info


def get_list_keys(path, trans):
    keys = []
    if path[-1] == '}':
        node = ncs.maagic.get_node(trans, path)
        child_cs_node = node._cs_node.children()
        while child_cs_node:
            if child_cs_node.is_key():
                keys.append(_ncs.hash2str(child_cs_node.tag()))
            child_cs_node = child_cs_node.next()
    return keys


def get_path_attr(path, trans, logger):
    ref_counter = 0
    ref_pointer = []
    try:
        attrs = trans.get_attrs([ATTR_REFCOUNT, ATTR_BACKPOINTER], path)
        if attrs:
            for attr in attrs:
                if attr.attr == ATTR_REFCOUNT:
                    ref_counter = int(attr.v)
                elif attr.attr == ATTR_BACKPOINTER:
                    for v in attr.v:
                        xpath = str(v)
                        keypath = str(_ncs.maapi.xpath2kpath(trans.maapi.msock, xpath))
                        ref_pointer.append(keypath)
    except Exception as ex:
        logger.info(f"Error in get_path_attr on path: {path}\nException: {ex}")
    return ref_counter, ref_pointer


def write_oper_data(device_name, len_set_leafs, len_not_ref_leafs, len_leaf_paths, diff, info, report_files, log):
    with ncs.maapi.single_write_trans('admin', 'system', db=ncs.OPERATIONAL) as tw:
        root = ncs.maagic.get_root(tw)
        oper_node = root.config_monitor.config_stats
        oper = oper_node.create(device_name)

        notif_time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.000")
        oper_stats = oper.inspection.create(notif_time)
        oper_stats.in_sync = False if diff else True
        oper_stats.leafs_in_service = len_set_leafs
        oper_stats.leafs_not_in_service = len_not_ref_leafs
        oper_stats.leafs_in_config = len_leaf_paths
        lines_unmanaged = 0
        if info:
            if 'Number of lines skipped' in info:
                # IOS XR device
                for line in info.split('\n', 3):
                    if 'Number of lines skipped' in line and ':' in line:
                        lines_unmanaged = int(line.split(':')[1].strip())
                        break
            elif 'unknown element: ' in info:
                # JUNOS
                lines_unmanaged = info.count('unknown element: ')
            else:
                # IOS XE
                lines_unmanaged = info.count('Line: ')
        oper_stats.config_lines_unmanaged = lines_unmanaged
        tw.apply()

    with ncs.maapi.single_write_trans('admin', 'system', db=ncs.OPERATIONAL) as trn:
        root = ncs.maagic.get_root(trn)
        stats_list = root.config_monitor.config_stats[device_name].inspection
        max_size = root.config_monitor.stats_size
        while len(stats_list) > max_size:
            keys = stats_list.keys()
            del stats_list[keys[0]]
        trn.apply()

    if report_files:
        with ncs.maapi.single_write_trans('admin', 'system', db=ncs.OPERATIONAL) as trn:
            root = ncs.maagic.get_root(trn)
            file_list = root.config_monitor.report_files
            for file in report_files:
                # log.info(f"{file.dir}, {file.name}, {file.size}, {file.time}")
                file_key = file.path
                if file_key in file_list:
                    file_oper = file_list[file_key]
                else:
                    file_oper = file_list.create(file_key)
                file_oper.size = file.size
                file_oper.time_created = datetime.fromtimestamp(file.time).strftime("%Y-%m-%dT%H:%M:%S.000")
            trn.apply()


def cleanup_oper_data(report_dir):
    with ncs.maapi.single_write_trans('admin', 'system', db=ncs.OPERATIONAL) as trn:
        root = ncs.maagic.get_root(trn)
        file_list = root.config_monitor.report_files
        files_to_delete = []
        for key in file_list:
            file_name = key.path.rsplit('/', 1)[1]
            file_path = os.path.join(report_dir, file_name)
            if not os.path.exists(file_path):
                files_to_delete.append(key.path)
        if files_to_delete:
            for key in files_to_delete:
                del file_list[key]
            trn.apply()


def is_primitive(obj):
    return not hasattr(obj, '__dict__')


def parse_children(path_dict, node_path, tr):
    n = ncs.maagic.get_node(tr, node_path)
    for child in n:
        (prefix, name) = child.split(':')
        if name in n:
            cn = n[name]
        elif hasattr(n, name):
            cn = getattr(n, name)
        else:
            attr = f"{prefix.replace('-', '_')}__{name}"
            if hasattr(n, attr):
                cn = getattr(n, attr)
            else:
                continue
        if cn is None:
            continue
        if is_primitive(cn):
            leaf_path = f"{node_path}/{name}"
            path_dict[leaf_path] = cn
        elif isinstance(cn, ncs.maagic.EmptyLeaf) and cn.exists():
            path_dict[cn._path] = ''
        elif isinstance(cn, ncs.maagic.Enum):
            leaf_path = f"{node_path}/{name}"
            path_dict[leaf_path] = cn.string
        elif isinstance(cn, ncs.maagic.LeafList) and cn.exists():
            for value in cn.as_list():
                leaf_list_elem_path = "%s{%s}" % (cn._path, value)
                path_dict[leaf_list_elem_path] = value
        elif (isinstance(cn, ncs.maagic.Container) and not isinstance(cn, ncs.maagic.PresenceContainer) or
                isinstance(cn, ncs.maagic.PresenceContainer) and cn.exists()):
            if '/yanglib:' in cn._path: continue
            if cn._path[-4:] == '-MIB': continue
            parse_children(path_dict, cn._path, tr)
        elif isinstance(cn, ncs.maagic.List):
            if '/yanglib:' in cn._path: continue
            for elem in cn:
                # path_dict[elem._path] = None
                parse_children(path_dict, elem._path, tr)


class FileInfo(object):
    def __init__(self, path_, size_, time_):
        self.path = path_
        self.size = size_
        self.time = time_


def write_to_file(report_dir, name, text, kind, report_list):
    cwd = os.path.abspath(os.getcwd())
    if report_dir.startswith('/'):
        file_dir = report_dir
    else:
        file_dir = os.path.join(cwd, report_dir)
    if not os.path.isdir(file_dir):
        os.mkdir(file_dir)
    file_name = f"{name}.{kind}"
    file_path = os.path.join(file_dir, file_name)
    if os.path.exists(file_path):
        os.remove(file_path)
    with open(file_path, "w") as f:
        f.write(text)
    if report_dir.endswith('webui/config-inspection-reports'):
        # Create a copy of the report in the state directory
        state_report_dir = os.path.join(Path(__file__).parent.parent.parent,
                                        'webui', 'config-inspection-reports')
        shutil.copy2(file_path, state_report_dir)
        file_stat = os.stat(file_path)
        report_list.append(FileInfo(os.path.join('config-inspection-reports', file_name),
                                    file_stat.st_size, file_stat.st_ctime))
    else:
        file_stat = os.stat(file_path)
        report_list.append(FileInfo(file_path, file_stat.st_size, file_stat.st_ctime))


# ---------------------------------------------------------
# Getting device running config
# ---------------------------------------------------------
def recv_all_and_close(c_sock):
    data = ''
    while True:
        buf = c_sock.recv(4096)
        if buf:
            data += buf.decode('utf-8')
        else:
            c_sock.close()
            return data


def read_config(m, th, path, dev_flag):
    dev_flags = (_ncs.maapi.CONFIG_CDB_ONLY +
                 _ncs.maapi.CONFIG_UNHIDE_ALL +
                 dev_flag)
    c_id = _ncs.maapi.save_config(m.msock, th, dev_flags, path)
    c_sock = socket.socket()
    (ncsip, ncsport) = m.msock.getpeername()
    _ncs.stream_connect(c_sock, c_id, 0, ncsip, ncsport)
    data = recv_all_and_close(c_sock)
    return data


def read_device_config(device_name):
    with ncs.maapi.Maapi() as m:
        with ncs.maapi.Session(m, 'admin', 'system'):
            with m.start_read_trans() as trans:
                root = ncs.maagic.get_root(trans)
                device = root.devices.device[device_name]
                ned_id = get_device_ned_id(device)
                dev_flag = None
                if ned_id:
                    if 'cisco-ios-cli' in ned_id:
                        dev_flag = _ncs.maapi.CONFIG_C_IOS
                    elif 'cisco-iosxr-cli' in ned_id:
                        dev_flag = _ncs.maapi.CONFIG_C
                    elif 'juniper-junos-nc' in ned_id:
                        dev_flag = _ncs.maapi.CONFIG_J
                    elif 'cisco-iosxr-nc' in ned_id:
                        dev_flag = _ncs.maapi.CONFIG_C
                if dev_flag:
                    dev_config = read_config(m, trans.th, device.config._path, dev_flag)
                    return dev_config
                else:
                    return None


# ---------------------------------------------------------
# Coloring running config
# ---------------------------------------------------------
class ConfigNode(object):
    def __init__(self, line_):
        self.line = line_.strip()
        self.line_compare = " ".join(line_.split())
        self.children = []
        self.color = ''
        self.parent = None


def config_reader(config_):
    config_list_ = config_.split('\n')
    complete_line = ''
    for line_ in config_list_:
        complete_line += line_
        if complete_line.count('"') % 2:
            continue

        yield complete_line
        complete_line = ''


def config_to_tree(current, gen_, logger, indent=0):
    line_ = next(gen_, '')
    while line_:
        node = ConfigNode(line_)
        strip_line = node.line
        line_indent = len(line_) - len(strip_line)
        if line_indent < indent:
            if current.parent:
                current = current.parent
            else:
                logger.info(f"ERROR in config_to_tree: current='{current.line}', line='{line_}'")
            current.children.append(node)
            node.parent = current
            indent -= 1
        elif line_indent == indent:
            current = current.parent
            current.children.append(node)
            node.parent = current
            current = node
            # indent = line_indent
        elif line_indent > indent:
            current.children.append(node)
            node.parent = current
            current = node
            indent += 1
        line_ = next(gen_, '')
    while current.parent:
        current = current.parent
    return current


def tree_to_config(root, indent=''):
    html_line = root.line if not root.color else f'<font color="red">{root.line}</font>'
    if root.line in ['!', '!\n!']:
        config_line = f"{indent[1:]}{html_line}\n"
    else:
        config_line = f"{indent}{html_line}\n"
    for child in root.children:
        config_line += tree_to_config(child, indent + ' ')
    return config_line


def match_and_color_children(config_, match, logger):
    for match_child in match.children:
        if match_child.line == '!':
            continue
        matched = False
        for config_child in config_.children:
            if config_child.line_compare == match_child.line_compare:
                config_child.color = "red"
                match_and_color_children(config_child, match_child, logger)
                matched = True
                break
        if not matched:
            logger.info(f"Config line not matched: {match_child.line}")


def get_start_node(root, start='config'):
    if root.line == start:
        return root
    for child in root.children:
        start_node = get_start_node(child, start)
        if start_node:
            return start_node
    return None


def color_config(running, match, logger, start='config'):
    config_start_node = get_start_node(running, start)
    match_start_node = get_start_node(match, start)
    if config_start_node and match_start_node:
        match_and_color_children(config_start_node, match_start_node, logger)


def color_j_config(config, match, logger):
    for match_statement in match.statements:
        matched = False
        for config_statement in config.statements:
            if config_statement.line_compare == match_statement.line_compare:
                config_statement.color = "red"
                matched = True
                break
        if not matched:
            logger.info(f"Config statement not matched: {match_statement}")

    for match_name, match_subsection in match.sub_sections.items():
        if match_name in config.sub_sections:
            config_subsection = config.sub_sections[match_name]
            config_subsection.color = "red"
            color_j_config(config_subsection, match_subsection, logger)
        else:
            logger.info(f"Config sub-section not matched: {match_subsection}")


# ---------------------------------------------
# Junos config parser
# ---------------------------------------------
class Statement(str):
    def __init__(self, item):
        self.line_compare = " ".join(item.split())
        self.color = ''

    def __str__(self):
        return super().__str__() + ";"

    def html_str(self):
        statement = self.__str__()
        html_line = statement if not self.color else f'<font color="red">{statement}</font>'
        return html_line


class Section:
    def __init__(self, name=None, items=None):
        self.name = name
        self.statements = set()
        self.sub_sections = OrderedDict()
        self._process_items(items)
        self.color = ''

    def __eq__(self, other):
        is_same = (
                self.statements == other.statements and
                self.name == other.name and
                self.sub_sections.keys() == other.sub_sections.keys()
        )
        if not is_same:
            return False
        else:
            for x in self.sub_sections.keys():
                if not (self.sub_sections[x] == other.sub_sections[x]):
                    return False
        return True

    def merge(self, other_section):
        self.statements.update(other_section.statements)
        for section in other_section.sub_sections:
            if section in self.sub_sections:
                self.sub_sections[section].merge(other_section.sub_sections[section])
            else:
                self.sub_sections[section] = other_section.sub_sections[section]

    def _process_items(self, items):
        for item in items:
            if isinstance(item, list):
                sub_section = Section(item[0], item[1])
                if self.sub_sections.get(sub_section.name):
                    self.sub_sections[sub_section.name].merge(sub_section)
                else:
                    self.sub_sections[sub_section.name] = sub_section
            else:
                statement = Statement(item)
                self.statements.add(statement)

    def __str__(self):
        statements = [str(x) for x in sorted(self.statements)]
        sections = [str(x) for x in self.sub_sections.values()]

        body = "\n".join(statements + sections)

        if self.name:
            body = textwrap.indent(body, " " * 4)
            return "%s {\n%s\n}" % (self.name, body)
        else:
            return body

    def html_str(self):
        statements = [x.html_str() for x in sorted(self.statements)]
        sections = [x.html_str() for x in self.sub_sections.values()]

        body = "\n".join(statements + sections)

        if self.name:
            body = textwrap.indent(body, " " * 4)
            return "%s {\n%s\n}" % (self.name, body)
        else:
            return body


class JunoConfig(Section):
    def __init__(self, filename=None, config=None):
        parser = self._build_parser()
        if filename:
            result = parser.parseFile(filename, parseAll=True)
        else:
            result = parser.parseString(config, parseAll=True)
        super().__init__(items=result.asList())

    def _build_parser(self):
        symbols = ":/.-_<*>$@"
        value = pp.dblQuotedString() | pp.Word(pp.alphas + pp.nums + symbols)
        line_comment = pp.Suppress(pp.Optional(pp.pythonStyleComment))
        multi_value = ('[' + pp.OneOrMore(value) + "]")

        statement = (
                pp.Combine(
                    pp.OneOrMore(value) + pp.Optional(multi_value),
                    joinString=" ", adjacent=False
                ) +
                pp.Suppress(";") +
                line_comment
        )

        section_name_part = pp.Word(pp.alphas + pp.nums + symbols)
        section_name = pp.Combine(pp.OneOrMore(section_name_part), joinString=" ", adjacent=False)

        section = pp.Forward()
        section <<= pp.Group(
            section_name +
            pp.Suppress("{") +
            line_comment +
            pp.Group(
                pp.ZeroOrMore(
                    pp.Suppress(pp.cStyleComment) |
                    statement |
                    section
                )
            ) +
            pp.Suppress("}") +
            line_comment +
            pp.Optional(pp.Suppress(pp.cStyleComment))
        )
        config = pp.OneOrMore(
            pp.Suppress(pp.cStyleComment) |
            section |
            statement
        )
        return config


# ---------------------------------------------
# COMPONENT THREAD THAT WILL BE STARTED BY NCS.
# ---------------------------------------------
class InspectConfigMain(ncs.application.Application):
    def setup(self):
        # The application class sets up logging for us. It is accessible
        # through 'self.log' and is a ncs.log.Log instance.
        self.log.info('InspectConfigMain RUNNING')

        # Service callbacks require a registration for a 'service point',
        # as specified in the corresponding data model.
        #
        self.register_service('inspect-config-action', InspectConfig)

        # If we registered any callback(s) above, the Application class
        # took care of creating a daemon (related to the service/action point).

        # When this setup method is finished, all registrations are
        # considered done and the application is 'started'.

    def teardown(self):
        # When the application is finished (which would happen if NCS went
        # down, packages were reloaded or some error occurred) this teardown
        # method will be called.

        self.log.info('InspectConfigMain FINISHED')


if __name__ == '__main__':
    device_name = 'ios'
    start_path = "/devices/device{%s}/config" % device_name
    leaf_paths = {}
    with ncs.maapi.single_read_trans('admin', 'system') as tr:
        parse_children(leaf_paths, start_path, tr)
    for path in leaf_paths:
        print(path, leaf_paths[path])
