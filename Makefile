all:
	ncs-setup --dest .
	$(MAKE) -C packages/config-monitor/src all

clean:
	$(MAKE) -C packages/config-monitor/src clean
	rm -rf config-inspection-reports

stop:
	ncs --stop

reset:
	ncs-setup --reset

cli:
	ncs_cli -u admin
